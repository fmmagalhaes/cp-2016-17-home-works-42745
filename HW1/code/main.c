#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "config.h"
#include "game.h"

int main(int argc, char *argv[]){
    
    GameConfig* gc = game_config_new_from_cli(argc, argv);
    Game* g = game_new();
   
    game_parse_board(g, gc);
    
    
    if(!gc->silent){
        printf("0\n");
        game_print_board(g);
        printf("\n\n");
    }
    
    int i;
    for(i = 0; i<gc->generations && !game_tick(g); i++){
        if(gc->debug){
            printf("%d\n", i+1);
            game_print_board(g);
            printf("\n\n");
        }
    }
    if((!gc->debug || i == 0) && !gc->silent){
        printf("%d\n", i);
        game_print_board(g);
        printf("\n\n");
    }
    game_free(g);
    game_config_free(gc);
    return 0;
}
#define DEFAULT_GEN 20;

/**
 * Set of structures and functions to parse and manage the
 * program's configuration options.
 */

#include <unistd.h>
#include <string.h>
#include "config.h"
#include <assert.h>


/**
 * Frees memory allocated for a GameConfig structure.
 *
 * @param config Pointer to a GameConfig structure.
 */
 void game_config_free(GameConfig *config){
  //free(config->generations);
  free(config);
}

/**
 * Returns the number of generations for which to run the game.
 *
 * @param config Pointer to a GameConfig structure.
 *
 * @return The number of generations for which to run.
 */
size_t game_config_get_generations(GameConfig *config){
  return config->generations;
}

/**
 * Parses the command line and create a new GameConfig from it.
 *
 * @param argc Number of command line arguments.
 * @param argv Array of command line arguments.
 *
 * @return A new GameConfig pointer.
 */
GameConfig *game_config_new_from_cli(int argc, char *argv[]){
  GameConfig* config = (GameConfig*) malloc (sizeof (GameConfig));
  char* filename;
  FILE* fp;
  
  config->generations = (size_t) DEFAULT_GEN;
  config->debug = 0;
  config->silent = 0;
  
  int c;
  int i = 0;
  while ((c = getopt (argc, argv, "d::n:s::")) != -1){
      i++;
     // printf("arg[%d]: ", i);
     // printf("flag = ");
      switch(c){
          case 'n':
              //printf("n");
              config->generations = (size_t) atoi(optarg);
          break;
          case 'd':
              //printf("d");
              config->debug = 1;
          break;
          case 's':
              //printf("s");
              config->silent = 1;
          break;
      }
      //printf(" val = %s\n",  optarg);
    }
    
    filename = argv[optind];
    fp = fopen(filename, "r");
    assert(fp!=NULL);
    config->input_file = fp;
    
    //printf("filename = %s\n", filename);
    
    return config;
    
}

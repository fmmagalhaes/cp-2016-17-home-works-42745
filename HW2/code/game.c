#include "game.h"
#include <string.h>
#include <cilk/cilk.h>
#include <assert.h>

/**
 * Frees memory allocated to a Game structure.
 *
 * @param game Pointer to be freed.
 */
void game_free(Game *game){
  free(game->board);
  free(game);
}

/**
 * Checks whether a given board position is in an alive state.
 *
 * @param game Pointer to a Game structure.
 * @param row  The row number.
 * @param col  The column number.
 *
 * @retval 0 The position is in a dead state.
 * @retval 1 The position is in an alive state.
 */
int game_cell_is_alive(Game *game, size_t row, size_t col){
  
  char *board = game->board;
  
  board+=game->cols*row;
  board+=col;
  
  return *board=='0'?0:1;
}

/**
 * Checks whether a given board position is in a dead state.
 *
 * @param game Pointer to a Game structure.
 * @param row  The row number.
 * @param col  The column number.
 *
 * @retval 0 The position is in an alive state.
 * @retval 1 The position is in a dead state.
 */
int game_cell_is_dead(Game *game, size_t row, size_t col){
    return !game_cell_is_alive(game, row, col);
}

/**
 * Allocates memory for a new Game structure.
 *
 * @return A new Game pointer.
 */
Game *game_new(void){
  Game* game = (Game*) malloc (sizeof (Game));
  //game->board = (char*) malloc(sizeof(char));
  return game;
  
}

/**
 * Parses a board file into an internal representation.
 *
 * Currently, this function only parses the custom file format
 * used by the program, but it should be trivial to add other
 * file formats.
 *
 * @param game Pointer to a Game structure.
 * @param config Pointer to a GameConfig structure.
 *
 * @retval 0 The board file was parsed correctly.
 * @retval 1 The board file could not be parsed.
 */
int game_parse_board(Game *game, GameConfig *config){

  char header[12];
  const char delimiters[2] = ":";
  char *token = "";

  FILE *input_file = config->input_file;
  
  /*get rows*/
  fgets(header, 12, input_file);
  /* get the first token */
  token = strtok(header, delimiters);
  token = strtok(NULL, delimiters);
  size_t rows = atoi(token);
  //printf("Rows = %zd\n", rows);
  
  /*get columns*/
  fgets(header, 12, input_file);
  /* get the first token */
  token = strtok(header, delimiters);
  token = strtok(NULL, delimiters);
  size_t columns = atoi(token);
  //printf("Columns = %zd\n", columns);
  
  char* board = (char*)malloc(columns*rows*sizeof(char));
  char* board_aux = board;
  int c = 0;
  
  while((c = fgetc(input_file)) != EOF){
    if( (char) c == '\n') continue;
    *(board++) = ((char) c == '#'?'1':'0');
  }

  //printf("board = %s\n", board_aux);

  game->cols = columns;
  game->rows = rows;
  game->board = board_aux;
  
  fclose(input_file);

  return 0;
}


/**
 * Prints the current state of the board.
 *
 * @param game Pointer to a Game structure.
 */
void game_print_board(Game *game){
  
  size_t rows = game->rows;
  size_t columns = game->cols;
  
  char* board = game->board;
  
  size_t i, j;

  for(i = 0; i<rows; i++){
    for(j = 0; j<columns; j++)
      printf("%c", *(board++) == '1'?'#':'.');
    printf("\n");
  }
}

/**
 * Sets a specific position in the board to an alive state.
 *
 * @param game Pointer to a Game structure.
 * @param row  The row number.
 * @param col  The column number.
 */
void game_cell_set_alive(Game *game, size_t row, size_t col){
  
  char *board = game->board;
  
  board+=game->cols*row;
  board+=col;
  
  *board = '1';
}

/**
 * Sets a specific position in the board to a dead state.
 *
 * @param game Pointer to a Game structure.
 * @param row  The row number.
 * @param col  The column number.
 */
void game_cell_set_dead(Game *game, size_t row, size_t col){
  
  char *board = game->board;
  
  board+=game->cols*row;
  board+=col;
  
  *board = '0';
}

int count_live_neighbors(Game *game, size_t row, size_t column){
  
  int alive = 0;
  size_t rows = game->rows;
  size_t cols = game->cols;

  int i, j;
  int first_row = row-1;
  int last_row = row+1;
  //printf("%d\n", first_row);
  //printf("%d\n", last_row);
  
  int first_col = column-1;
  int last_col = column+1;
  //printf("%d\n", first_col);
  //printf("%d\n", last_col);
  

  //iterates through all 8 neighbors
  for(i = first_row; i<=last_row; i++){
    for(j = first_col; j<=last_col; j++){
      int x = j<0?(int)(cols-1):j%cols;
      int y = i<0?(int)(rows-1):i%rows;
      //printf("y = %d, x = %d\n", y, x);
      if(i == row && j == column) continue;
      alive+=game_cell_is_alive(game, y, x);
      //printf("is alive? %d\n", alive);
    }
  }
  
  return alive;
  
}


/**
 * Any cell with < 2 live neighbors dies (under-population) 
 * Any cell with 2 or 3 live neighbors lives to next gen.
 * Any cell with > 3 live neighbors dies (overcrowding);
 * Any dead cell with 3 live neighbors becomes a live cell;
 * Advances the cell board to a new generation (causes a 'tick').
 *
 * @param game Pointer to a Game structure.
 *
 * @retval 0 The tick has happened successfully.
 * @retval 1 The tick could not happen correctly.
 */
int game_tick(Game *game){
  
  int steady = 1;
  size_t rows = game->rows;
  size_t columns = game->cols;
  
  char* board = game->board;
  char* new_board = (char*)malloc(columns*rows*sizeof(char));

  cilk_for(size_t row = 0; row<rows; row++){
    cilk_for(size_t column = 0; column<columns; column++){
      
      int live_neighbors = 0;
      live_neighbors = count_live_neighbors(game, row, column);
      
      char* new_board_aux = new_board+(columns*row)+column;
      
      if(game_cell_is_alive(game, row, column)){
        if(live_neighbors<2 || live_neighbors>3)
          *new_board_aux = '0';
        else
          *new_board_aux = '1';
      }
      else{
        if(live_neighbors==3)
          *new_board_aux = '1';
        else
          *new_board_aux = '0';
      }
      if(steady && *(board+(columns*row)+column) != *new_board_aux)
        steady = 0;
        
      //printf("row = %d\n", row);
    }
  }
  
  free(game->board);
  game->board = new_board;
  
  return steady;
}
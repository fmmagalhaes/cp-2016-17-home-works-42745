package cp.sharedCounter;

import java.util.Random;
import org.deuce.Atomic;

public class Worker extends Thread {


	public static int counter = 0;

	private final StopVar sharedStop;
	private boolean transaction = false;
	
	private int numOps = 0;
	
	public Worker(StopVar sharedStop, boolean transaction) {
		super();
		this.sharedStop = sharedStop;
		this.transaction = transaction;
	}
	
	@Override
	public void run() {
		while(!sharedStop.stop) {
			if(transaction)
				inc();
			else
				inc2();
			numOps++;
		}
	}
	
	@Atomic
	public void inc(){
		counter++;
	}
	
	//not atomic
	public void inc2(){
		counter++;
	}
	
	public int getNumOps() {
		return numOps;
	}
}

package cp.sharedCounter;

import java.io.IOException;

public class Main {
	
	public static final boolean ATOMIC = Boolean.parseBoolean(System
			.getProperty("cp.sharedCounter.atomic"));
	
	public static void main(String[] args) throws InterruptedException, IOException {
		
		if (args.length < 2) {
			System.out.println("Usage: "+Main.class.getCanonicalName()+" time_in_milisecs num_threads");
			System.exit(0);
		}
		
		// get command line arguments
		Integer time = Integer.parseInt(args[0]);
		Integer numThreads = Integer.parseInt(args[1]);
		
		StopVar stopVar = new StopVar();
		Worker[] workers = new Worker[numThreads];
 		
		System.out.println("Starting application...");
		
		// launch the threads
		for (int i=0; i < numThreads; i++) {
			workers[i] = new Worker(stopVar, ATOMIC);
			workers[i].start();
		}
		
		// wait for the specified time
		Thread.sleep(time);
		stopVar.stop = true;
		
		// claculate statistics
		int sumOps = 0;
		
		for (int i=0; i < numThreads; i++) {
			workers[i].join();
			sumOps += workers[i].getNumOps();
		}
		
		
		// print statistics
		System.out.println("Total operations: "+sumOps);
		System.out.println("Counter: "+Worker.counter);
		System.out.println("Throughput: "+(Math.round(sumOps/(time/1000.0)))+" ops/sec");
		
		System.out.println("Finished application.");
		
	}
}
